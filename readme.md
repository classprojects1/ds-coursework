The question requires us to store related data that belong to a single student so we had to use a Structure .
This is because structures provide a clear way to group related data elements under a single name, making it easier to work with student data as a whole.
Each student's information is encapsulated within a well-defined structure, promoting code readability and maintainability.
we used an array over a linked linked list because Linked lists,require traversing the list from the beginning until the desired element is found.This traversal can be time-consuming for large lists.
An array allows for efficient storage and retrieval of a fixed number of student records.
Since the maximum number of students is known , an array provides direct access to any student's data using an index. This simplifies operations like displaying all students or accessing a specific student's details.

YOUTUBE LINKS FOR INDIVIDUAL GRUOP MEMBERS
TWOMO ELVIS RODNEY   REG NO.:23/U/1505
https://youtu.be/EqeAuZ3vgTY

ITIANIT JUDE   REG NO.:23/U/08403/PS
https://youtu.be/sn_qHEWSne8?si=65OE6avDPJySlOBr

MASAGAZI RUTH   REG NO.:23/U/11051/EVE
https://youtu.be/pMLbITQ1Ef0

NANTONGO JOSELYN MADRINE   REG NO.:23/U/15305/PS
https://youtu.be/B7Z7t3RgddE

TUGUME FAROUK   REG NO.:23/U/17998/EVE

REPOSITORY LINK
https://gitlab.com/classprojects1/ds-coursework.git