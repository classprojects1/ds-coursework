#include <stdio.h>
#include <string.h>

typedef struct student {
    char name[51];
    char dob[11];
    char regNo[7];
    char course[5];
    float annualTuition;
} Student;

void create_student(Student students[], int *student_count);
void read_students(Student students[], int student_count);
void update_student(Student students[], int student_count);
void delete_student(Student students[], int *student_count);
void search_student(const Student students[], int student_count);
void sortByName(Student students[], int student_count);
void sortByRegNumber(Student students[], int student_count);
void exportToCSV(Student students[], int student_count, const char *filename);

int main() {
    int MAX_STUDENTS = 100;
    Student students[MAX_STUDENTS];
    int student_count = 0;
    int choice;
    int choice2;
    const char *filename = "students.csv";


    while (1) {
        printf("STUDENTS RECORDS\n");
        printf("1. Create/Add\n");
        printf("2. Display Data\n");
        printf("3. Update\n");
        printf("4. Delete\n");
        printf("5. Search by RegNO.\n");
        printf("6. Sort\n");
        printf("7. Export\n");
        printf("8. Exit\n");
        printf("\n");
        printf("\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                create_student(students, &student_count);
                printf("\n");                
                break;
            case 2:
                read_students(students, student_count);
                printf("\n");                
                break;
            case 3:
                update_student(students, student_count);
                printf("\n");                
                break;
            case 4:
                delete_student(students, &student_count);
                printf("\n");                
                break;
            case 5:
                search_student(students, student_count);
                printf("\n");                
                break;
                
            case 6:                
                printf("Enter (1) to sort_by_Name or (2) to sort_ by_RegNo: \n ");                
                scanf("%d", &choice2);
                switch(choice2){
                    case 1:
                        sortByName(students, student_count);
                        printf("\n");                        
                        break;

                    case 2:
                        sortByRegNumber(students, student_count);
                        printf("\n");                        
                        break;
                        
                } 
                break;
            case 7:
                exportToCSV(students, student_count, filename);
                printf("\n");                
                break;

            case 8:
                printf("Exiting. Have a great day!\n");
                return 0;
            
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
// Function to create a student and add it to the array
void create_student(Student students[], int *student_count) {
    if (*student_count < 100) {
        printf("Enter student details:\n");
        
        printf("Name (max 50 characters): ");
        scanf("%51s", students[*student_count].name);

        printf("Registration number (max 6 characters): ");
        scanf("%7s", students[*student_count].regNo);   

        printf("Date of birth (YYYY-MM-DD): ");
        scanf("%11s", students[*student_count].dob);  

        printf("Course (max 4 characters): ");
        scanf("%5s", students[*student_count].course);  

        printf("Annual tuition: ");
        scanf("%f", &students[*student_count].annualTuition);

        (*student_count)++;
        printf("Student created successfully.\n");
    } else { 
        printf("Maximum student limit reached.\n");
    }
}

// Function to print all students in the system
void read_students(Student students[], int student_count) {
    printf("List of Students:\n");
    for (int i = 0; i < student_count; i++) {
        printf("No: %d.", i + 1);
        printf("[");
        printf("Name: %s, ", students[i].name);
        printf("Date of birth: %s, ", students[i].dob);
        printf("RegNo: %s, ", students[i].regNo);
        printf("Course: %s, ", students[i].course);
        printf("Annual tuition: Shs %.2f. ", students[i].annualTuition);
        printf("]");;
        printf("\n");
    }
}

// Function to update a student
void update_student(Student students[], int student_count) {
    char id[7]; 
    int found = 0;

    printf("Enter student Registration number to update: ");
    scanf("%s", id);

    for (int i = 0; i < student_count; i++) {
        if (strcmp(students[i].regNo, id) == 0) {
            found = 1;
            printf("Student found. Enter the field to update:\n");
            printf("1. Name\n");
            printf("2. Date of birth\n");
            printf("3. Registration number\n");
            printf("4. Course\n");
            printf("5. Annual tuition\n");

            int choice;
            printf("Enter your choice (1-5): ");
            scanf("%d", &choice);

            switch (choice) {
                case 1:
                    printf("Enter new student name: ");
                    scanf("%s", students[i].name);
                    break;
                case 2:
                    printf("Enter new date of birth (YYYY-MM-DD): ");
                    scanf("%s", students[i].dob);
                    break;
                case 3:
                    printf("Enter new registration number: ");
                    scanf("%s", students[i].regNo);
                    break;
                case 4:
                    printf("Enter new course code: ");
                    scanf("%s", students[i].course);
                    break;
                case 5:
                    printf("Enter new annual tuition: ");
                    scanf("%f", &students[i].annualTuition);
                    break;
                default:
                    printf("Invalid choice. No fields updated.\n");
            }

            printf("Student updated successfully.\n");
            break;
        }
    }

    if (!found) {
        printf("Student not found.\n");
    }
}

// Function to delete a student from the array
void delete_student(Student students[], int *student_count) {
    char id[7];
    int found = 0;

    printf("Enter student Registration number to delete: ");
    scanf("%s", id);

    for (int i = 0; i < *student_count; i++) {
        if (strcmp(students[i].regNo, id) == 0) {
            // Shift remaining students to fill the gap
            for (int j = i; j < *student_count - 1; j++) {
                students[j] = students[j + 1];
            }
            (*student_count)--;
            printf("Student deleted successfully.\n");
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student not found.\n");
    }
}

// Function to search for a student by registration number
void search_student(const Student students[], int student_count) {
    char id[7]; 
    int found = 0;

    printf("Enter student Registration number to search: ");
    scanf("%s", id);

    for (int i = 0; i < student_count; i++) {
        if (strcmp(students[i].regNo, id) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dob);
            printf("Registration Number: %s\n", students[i].regNo);
            printf("Course: %s\n", students[i].course);
            printf("Annual Tuition: %.2f\n", students[i].annualTuition);
            found = 1;
            break;
        }
    }

    if (!found) {
        printf("Student not found.\n");
    }
}

// Function to sort student by Name
void sortByName(Student students[], int student_count) {
    Student temp;
    for (int i = 0; i < student_count - 1; i++) {
        for (int j = i + 1; j < student_count; j++) {
            if (strcmp(students[i].name, students[j].name) > 0) {
                temp = students[i];
                students[i] = students[j];
                students[j] = temp;
            }
        }
    }

    // Print the sorted names
    printf("Sorted names:\n");
    printf("[");
    for (int i = 0; i < student_count; i++) {
        printf("%s", students[i].name);
        if (i < student_count - 1) {
            printf(", ");
        }
    }
    printf(" ] \n");
}


// Function to sort student by Registration Number
void sortByRegNumber(Student students[], int student_count) {
    Student temp;
    for (int i = 0; i < student_count - 1; i++) {
        for (int j = i + 1; j < student_count; j++) {
            if (strcmp(students[i].regNo, students[j].regNo) > 0) {
                temp = students[i];
                students[i] = students[j];
                students[j] = temp;
            }
        }
    }

    // Print the sorted registration numbers
    printf("Sorted registration numbers:\n");
    printf("[");
    for (int i = 0; i < student_count; i++) {
        printf("[%s]", students[i].regNo);
        if (i < student_count - 1) {
            printf(", ");
        }
    }
    printf(" ] \n");
}


// Function to export students to CSV FILE
void exportToCSV(Student students[], int student_count, const char *filename) {
    FILE *file = fopen(filename, "a"); // Open the file in append mode
    if (file == NULL) {
        printf("Error opening file!\n");
        return;
    }

    for (int i = 0; i < student_count; i++) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n",
                students[i].name, students[i].dob, students[i].regNo,
                students[i].course, students[i].annualTuition);
    }

    fclose(file);
    printf("Records have been successfully exported to %s\n", filename);
}




